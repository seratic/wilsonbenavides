import { Product } from './models/product.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  arrProducts: Product[]
  baseUrl: string

  constructor(
    private http: HttpClient
  ) {
    this.arrProducts = []
    this.baseUrl = './assets/products.json'
  }

  insert(pProduct): void {
    this.arrProducts.push(pProduct)
    console.log(this.arrProducts)
  }

  getAll(): Promise<Product[]> {
    return new Promise((resolve, reject) => {
      resolve(this.arrProducts)
    })
  }

  getAllFromJson(): Promise<Product[]> {
    return this.http.get<Product[]>(this.baseUrl).toPromise()
  }
}
