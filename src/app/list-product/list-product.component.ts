import { Product } from './../models/product.model';
import { ProductsService } from './../products.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss']
})
export class ListProductComponent implements OnInit {

  products: Product[]

  constructor(
    private productService: ProductsService
  ) { }

  ngOnInit(): void {

    this.productService.getAllFromJson()
    .then(response => {
      this.products = response['results']
      console.log(this.products)
    })
    this.productService.getAll()
    .then(arrProducts => {
      this.products = arrProducts
      console.log(arrProducts)
    })
  }

}
