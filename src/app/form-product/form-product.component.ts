import { ProductsService } from './../products.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-product',
  templateUrl: './form-product.component.html',
  styleUrls: ['./form-product.component.scss']
})
export class FormProductComponent implements OnInit {

  formNewProduct: FormGroup;

  constructor(
    private productsService: ProductsService
  ) {
    this.formNewProduct = new FormGroup({
      name: new FormControl('', [
        Validators.required
      ]),
      description: new FormControl('', [
        Validators.maxLength(250)
      ]),
      price: new FormControl('', [
        Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)
      ]),
      image: new FormControl(''),
      discount: new FormControl('', [
        Validators.max(40)
      ])
    })
  }

  ngOnInit(): void {
  }

  onSubmit() {
    console.log(this.formNewProduct.value)
    this.productsService.insert(this.formNewProduct.value)
  }

}
