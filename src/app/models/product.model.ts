export class Product {
  name: string
  description: string
  price: string
  image: string
  discount: string
  price_with_discount: string
}
